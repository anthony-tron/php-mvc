<?php

require_once 'app/Controller.php';

class Home extends Controller {

    public function default($params) {
        $template = new View('template');
        $homeView = new View('home');

        $template->render(['title' => 'Hello world', 'body' => $homeView->capture()]);
    }
}