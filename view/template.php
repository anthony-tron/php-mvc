<!doctype HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title><?= $data['title'] ?></title>
</head>
<body>
<?= isset($data['header']) ? $data['header'] . PHP_EOL : ''  ?>
<?= $data['body'] . PHP_EOL ?>
<?= isset($data['footer']) ? $data['footer'] : '' ?>
</body>
</html>