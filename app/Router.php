<?php

require 'Controller.php';

class Router {

    public function route() {

        $params = explode('/', $_GET['params']);

        try {
            $controllerName = $this->filterControllerName($params[0]);

        } catch(Exception $e) {
            header('Location: /' . BASE . '/home');

        } finally {
            require 'controller/' . $controllerName . '.php';
            $controller = new $controllerName();

            $action = isset($params[1]) && method_exists($controller, $params[1]) ? $params[1] : 'default';

            $controller->$action($params);
        }
    }


    /**
     * Returns the controller name that resembles to $str.
     * For instance, if $str is 'signin' and the controller 'SignIn' is found, it will return 'SignIn'
     * @param $str
     * @return string
     * @throws Exception if $str is not a controller
     */
    private function filterControllerName($str) {
        if ($str = (string) filter_var($str)) {
            foreach (Controller::validControllers() as $validControllerName) {
                if (strcasecmp($str, $validControllerName) == 0) {
                    return $validControllerName;
                }
            }
        }

        throw new Exception('Invalid controller: ' . $str);
    }
}