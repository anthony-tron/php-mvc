<?php

abstract class Model {

    const DATABASE_NAME = 'DATABASE_NAME';
    const DATABASE_HOST = 'DATABASE_HOST';
    const DATABASE_USER = 'DATABASE_USER';
    const DATABASE_PASSWORD = 'DATABASE_PASSWORD';

    protected $connexion;

    public function __construct($table) {
        $this->table = $table;

        $dsn = 'mysql:dbname=' . self::DATABASE_NAME . ';host=' . self::DATABASE_HOST;

        $this->connexion = new PDO($dsn, self::DATABASE_USER, self::DATABASE_PASSWORD);
        $this->connexion->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }
}