<?php

require 'View.php';

abstract class Controller {

    /**
     * Returns an array of the valid controllers' names
     * @return array
     */
    static public function validControllers() {
        $allFiles = array_diff(scandir('controller'), array('..', '.'));
        return array_map(fn($e) => substr($e, 0, strrpos($e, '.')), $allFiles);
    }

    abstract public function default($params);
}
