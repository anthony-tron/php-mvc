<?php


class View {

    private $name;

    /**
     * View constructor.
     * @param string $name the name of the file without extensions ; must be located in view/
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * Outputs the view with custom data
     * @param array $data the custom data accessible by the view
     */
    public function render($data = array()) {
        include 'view/' . $this->name . '.php';
    }

    /**
     * Returns the view with custom data as a string
     * @param array $data the custom data accessible by the view
     * @return string the string representation of the view
     */
    public function capture($data = array()) {
        ob_start();
        $this->render($data);
        return ob_get_clean();
    }
}